
{
    "name": "MBF Theme",

    'sequence': -10,

    "description": """MobiFone Theme""",

    "summary": "MobiFone Theme",

    "category": "Themes/Backend",

    "version": "0.1",

    'author': "MobifoneIT - PTPM3",

    'website': "https://dev3.1erp.vn",
    'company': 'MobifoneIt - PM3',
    'maintainer': 'MobifoneIt -PM3',
    "depends": ['base', 'web', 'mail', 'base_setup', 'im_livechat_mail_bot', 'auth_signup', 'base_import'],
    "data": [
        'views/layout.xml',
        'views/login_signup.xml',
		'views/custom_footer.xml',
    ],

    'assets': {
        'web.assets_frontend': [
            'mobifone_theme/static/src/scss/login.scss',
            'mobifone_theme/static/src/scss/website_layout.scss',
            'mobifone_theme/static/src/js/custom_login/custom_login.js',
        ],
        'web.assets_backend': [
            'mobifone_theme/static/src/scss/theme_accent.scss',
            'mobifone_theme/static/src/scss/navigation_bar.scss',
            'mobifone_theme/static/src/scss/datetimepicker.scss',
            'mobifone_theme/static/src/scss/theme.scss',
            'mobifone_theme/static/src/scss/sidebar.scss',
            'mobifone_theme/static/src/scss/theme_responsive.scss',
            'mobifone_theme/static/src/js/fields/colors.js',
            'mobifone_theme/static/src/js/chrome/sidebar_menu.js',
            'mobifone_theme/static/src/js/custom_menu_items.js',
        ],
        'web.assets_qweb': [
            'mobifone_theme/static/src/xml/top_bar.xml',
        ],
    },
    'license': 'LGPL-3',

    'pre_init_hook': 'pre_init_hook',

    'post_init_hook': 'post_init_hook',

    'installable': True,

    'application': True,
    
    'auto_install': True,
}
