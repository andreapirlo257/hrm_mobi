odoo.define("mobifone_theme.SidebarMenu", function (require) {
  "use strict";

  //sidebar toggle effect
  $(document).on("click", "#closeSidebar", function (event) {
    $("#closeSidebar").hide();
    $("#openSidebar").show();
  });
  $(document).on("click", "#openSidebar", function (event) {
    $("#openSidebar").hide();
    $("#closeSidebar").show();
  });
  $(document).on("click", "#openSidebar", function (event) {
    $("#sidebar_panel").css({ display: "block" });
  });
  $(document).on("click", "#closeSidebar", function (event) {
    $("#sidebar_panel").css({ display: "none" });
  });

  $(document).on("click", ".sidebar a", function (event) {
    var menu = $(".sidebar a");
    var $this = $(this);
    var id = $this.data("id");
    $("header").removeClass().addClass(id);
    menu.removeClass("active");
    $this.addClass("active");

    //sidebar close on menu-item click
    $("#sidebar_panel").css({ display: "none" });
    $(".o_action_manager").css({ "margin-left": "0px" });
    $("#closeSidebar").hide();
    $("#openSidebar").show();
  });
});
