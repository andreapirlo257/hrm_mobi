{
    "name": "Human Resource",
    "version": "2.0", 
    "category": "HumanResource",
    "summary": "Human Resource management system",
    'sequence': -102,
    "description": "Human Resource management system",
    "depends": ['mail', 'hr', 'hr_contract','hr_holidays', 'hr_attendance'],
    "data": [
        'security/ir.model.access.csv',
        'views/employee_information_general.xml',
        'views/employee_information_family.xml',
        'views/employee_insurance_info_lines.xml',
        'views/employee_insurance_view.xml',
        'views/employee_job.xml',
        'views/employee_contract.xml',
        'views/employee_work_experience.xml',
        'report/report_contract.xml'
    ],
    "demo": [],
    'installable': True,
    "application": True,
    "auto_install": False,
    'assets': {
        'web.assets_frontend': [
            'employee/static/src/scss/report_contract.scss'],
        'web.assets_backend': [
            'employee/static/src/scss/employee_information_general.scss',
            'employee/static/src/scss/em_insurance.scss',
        ],
        'web.assets_qweb': [
    
        ],
        'web.report_assets_common': [
            'employee/static/src/scss/report_contract.scss',
        ],
    },
    'post_init_hook':"",
    "license": "LGPL-3"
    
}