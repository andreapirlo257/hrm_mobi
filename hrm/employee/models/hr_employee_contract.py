import re
from datetime import date

from odoo import _, api, fields, models


class HrEmployeeInherit(models.Model):
    _name="hr.contract"
    _inherit="hr.contract"

    employee_id = fields.Many2one('hr.employee', string='Employee')
    branch_contract = fields.Char(string="Chi nhánh")
    work_location_contract = fields.Char(string ="Địa điểm làm việc")