from . import (hr_employee_contract, hr_employee_information_family,
               hr_employee_information_general, hr_employee_insurance,
               hr_employee_insurance_info_lines, hr_employee_work_experience)
